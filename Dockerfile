FROM nginx:mainline-alpine3.18

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY package-lock.json ./

COPY ./build /usr/share/nginx/html/


# Expose port 80 to the outside world
EXPOSE 80

RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent

COPY . ./

CMD ["npm" ,"start"]
