import axios from "axios";

export default axios.create({
  baseURL: "http://tutorials-be:8080/tutorials-be/api",
  headers: {
    "Content-type": "application/json"
  }
});